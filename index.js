"use strict";

var client = require('socket.io-client'),
    commandLineArgs = require('command-line-args'),
    getUsage = require('command-line-usage');
const notifier = require('node-notifier');
const path = require('path');

var config, phoneNumber, pin;

var run = function() {

    var socket = client(config.url)

    socket.on('connect', function() {
        socket.emit('waitfor', {phone: phoneNumber, pin: pin});
    });

    socket.on('nofity', function(data) {
        console.log('notified', data);
        var caller = (data.caller_name) ? data.caller_name : (data.caller) ? data.caller : 'Unkown';
        if (data.status == 'RINGING') {
            notifier.notify({
                title: 'Ringhio',
                message: caller + ' is calling',
                sound: true, // Only Notification Center or Windows Toasters
                wait: true // Wait with callback, until user action is taken against notification
            }, function(err, response) {
                // Response is response from notification
            });

            notifier.on('click', function(notifierObject, options) {
                // Triggers if `wait: true` and user clicks notification
            });

            notifier.on('timeout', function(notifierObject, options) {
                // Triggers if `wait: true` and notification closes
            });
        }
    });
}

var optionDefinitions = [{
    alias: 'c',
    name: 'config',
    description: 'Path to config json file',
    type: String
}, {
    alias: 'n',
    name: 'phone',
    description: 'Phone number',
    type: String
}, {
    alias: 'p',
    name: 'pin',
    description: 'PIN',
    type: String
}]

const options = {
    title: 'Ringhio node client',
    description: 'Ringhio node client'
}

var cli = commandLineArgs(optionDefinitions);
var args = cli.parse()

var lconfig = args.config
pin = args.pin
phoneNumber = args.phone

if (!lconfig || !pin || !phoneNumber) {
    console.log(getUsage(optionDefinitions, options))
    process.exit(1);
}

config = require(lconfig);

run();
